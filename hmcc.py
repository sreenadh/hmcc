# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import *

import psycopg2
from string import *

import procname, sys, locale, ConfigParser, datetime, time, os

from ui.hmcc_ui import Ui_hmcc
from functions import *

class Hmcc(QtGui.QMainWindow, Ui_hmcc, Functions):

    def __init__(self):
      QtGui.QMainWindow.__init__(self)
      self.setupUi(self)
      self.setWindowIcon(QtGui.QIcon('icon.png'))
      self.showMaximized()
      self.qleMessageBox.setVisible(False)
      self.setFont(QtGui.QFont("FreeSans",15,QtGui.QFont.Bold))

      self.connect(self.btnQuit,QtCore.SIGNAL('clicked()'),self.close)
      
      self.connect(self.btnMember,QtCore.SIGNAL('clicked()'),self.showMemberFunctions)
      self.connect(self.btnCollection,QtCore.SIGNAL('clicked()'),self.showCollectionFunctions)
      self.connect(self.btnAdmin,QtCore.SIGNAL('clicked()'),self.showAdminFunctions)

      self.actionFrame = None
      
      self.lblToday.setText("Date: " + today_str )
      self.lblWeek.setText("Week: " + str( current_week ) )
      self.lbFiscal.setText( startyear + "-" + endyear )
	  
    def execute_query(self, query, data=""):
      c = db.cursor()
      try:
        c.execute( query, data )
        db.commit()
      except Exception, e:
        print e
        db.rollback()
        return False
      else:
        return c
        
######################### Member functions #####################

    def showMemberFunctions(self):
      from member_actions import Member_Actions
      self.showActions( Member_Actions() )
      
      self.signal_connect(self.actionFrame.btnMemberAdd,self.showNewMember)
      self.signal_connect(self.actionFrame.btnMemberList,self.showMemberList)
      self.signal_connect(self.actionFrame.btnRip,self.showRIP)
      
      self.showBack(self.clear_Layout)
    
    def showRIP(self):
      from rip import RIP
      self.showActions(RIP())
      self.showBack(self.showMemberFunctions)
      
      self.actionFrame.qleBookNo.setFocus()
      reg = QtCore.QRegExp( "^[0-9]*$" )
      validator = QtGui.QRegExpValidator(reg, self);
      self.actionFrame.qleBookNo.setValidator( validator )
      self.actionFrame.qleAge.setValidator( validator )
      self.actionFrame.qleDate.setDate(today)
      
      self.signal_connect(self.actionFrame.btnSubmit,self.addRIP)
      self.signal_connect(self.actionFrame.btnDeathRecords,self.showRIPEntries)
    
    def addRIP(self):
      date = self.actionFrame.qleDate.date().toString(QtCore.Qt.ISODate)
      data = ( str(self.actionFrame.qleBookNo.text()), str(self.actionFrame.qleName.text()), str(self.actionFrame.qleAge.text()), str(self.actionFrame.pteAddress.toPlainText()), str(date), )
      if data[0]!="" and data[1] !="" and data[2] != "" and data[3] !="" :
        if self.checkMember(data[0]):
          query = """INSERT INTO rip(membershipno,name,age,address,date)
                    VALUES(%s,%s,%s,%s,%s)"""
          c = self.execute_query(query,data)
          if c!=False:
            self.showMessageBox("Entry added successfully")
            self.actionFrame.qleBookNo.setText("")
            self.actionFrame.qleAge.setText("")
            self.actionFrame.qleName.setText("")
            self.actionFrame.pteAddress.setPlainText("")
            self.actionFrame.qleBookNo.setFocus()
        else:
          self.showMessageBox("Member does not exists",2)
      else:
        self.showMessageBox("Fill all data", 2)
        self.actionFrame.qleBookNo.setFocus()

    def deleteRIPEntry(self):
      r = QtGui.QMessageBox.warning(self,"Death records","Delete entry?", QtGui.QMessageBox.Yes|QtGui.QMessageBox.Default, QtGui.QMessageBox.No|QtGui.QMessageBox.Escape)
      if r == QtGui.QMessageBox.Yes:
        query = "DELETE FROM rip WHERE id=%s"
        row = self.actionFrame.tbl_death_entries.currentRow()
        id = str( self.actionFrame.tbl_death_entries.item(row,0).text() )
        data3 = [ id, ]
        self.execute_query( query, data3 )
        self.showRIPEntries()
      
    def showRIPEntries(self):
      from rip_entries import Rip_Entries
      self.showActions( Rip_Entries() )
      self.showBack(self.showRIP)
      self.signal_connect(self.actionFrame.btnDelete,self.deleteRIPEntry)
      
      query = """SELECT r.id,r.date,r.membershipno,r.name,r.age
              FROM rip r
              ORDER BY date """
      c = self.execute_query( query ) 
      if c != False:
        count=0
        for d in c.fetchall():
          self.actionFrame.tbl_death_entries.setRowCount(count+1)
          
          self.actionFrame.tbl_death_entries.setItem(count,0,QtGui.QTableWidgetItem(QtCore.QString.number(d[0])))
          self.actionFrame.tbl_death_entries.setItem(count,1,QtGui.QTableWidgetItem(QtCore.QString.number(count+1)))
          dt = datetime.datetime.strptime(d[1],"%Y-%m-%d").strftime("%d-%m-%Y")
          self.actionFrame.tbl_death_entries.setItem(count,2,QtGui.QTableWidgetItem(dt))
          self.actionFrame.tbl_death_entries.setItem(count,3,QtGui.QTableWidgetItem(QtCore.QString.number(d[2])))
          self.actionFrame.tbl_death_entries.setItem(count,4,QtGui.QTableWidgetItem(d[3]))
          self.actionFrame.tbl_death_entries.setItem(count,5,QtGui.QTableWidgetItem(QtCore.QString.number(d[4])))
          
          self.actionFrame.tbl_death_entries.item(count,1).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_death_entries.item(count,3).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_death_entries.setColumnWidth(0,0)
          self.actionFrame.tbl_death_entries.setColumnWidth(2,120)
          self.actionFrame.tbl_death_entries.setColumnWidth(3,120)
          self.actionFrame.tbl_death_entries.setColumnWidth(4,300)
          
          self.actionFrame.tbl_death_entries.setRowHeight(count,50)
          count+=1
        
        if count>0:
          self.actionFrame.tbl_death_entries.setCurrentItem( self.actionFrame.tbl_death_entries.item(0,0) )
      
    def showNewMember(self):
      from member_new import Member_New
      self.showActions( Member_New() )
      self.showBack(self.showMemberFunctions)
      
      self.actionFrame.qleBookNumber.setFocus()
      reg = QtCore.QRegExp( "^[0-9]*$" )
      validator = QtGui.QRegExpValidator(reg, self);
      self.actionFrame.qleBookNumber.setValidator( validator )
      self.signal_connect(self.actionFrame.btnSubmit,self.addMember)
      
    def clearNewMemberForm(self):
      self.actionFrame.qleBookNumber.setText("")
      self.actionFrame.qleMemberName.setText("")
      self.actionFrame.qleHouseName.setText("")
      self.actionFrame.qleBookNumber.setFocus()
      self.actionFrame.qle1.setText("")
      self.actionFrame.qle2.setText("")
      self.actionFrame.qle3.setText("")
      self.actionFrame.qle4.setText("")
      self.actionFrame.qle5.setText("")
      self.actionFrame.qle6.setText("")
    
    def get_family_members(self):
      data1 = []
      if self.actionFrame.qle1.text() != "":
            data1 = [ str(self.actionFrame.qle1.text() ) ]
      if self.actionFrame.qle2.text() != "":
        data1.append( str(self.actionFrame.qle2.text() ) )
      if self.actionFrame.qle3.text() != "":
        data1.append( str(self.actionFrame.qle3.text() ) )
      if self.actionFrame.qle4.text() != "":
        data1.append( str(self.actionFrame.qle4.text() ) )
      if self.actionFrame.qle5.text() != "":
        data1.append( str(self.actionFrame.qle5.text() ) )
      if self.actionFrame.qle6.text() != "":
        data1.append( str(self.actionFrame.qle6.text() ) )
      return data1
            
    def addMember(self):
      data = [ str(self.actionFrame.qleBookNumber.text()), str(self.actionFrame.qleMemberName.text()),str(self.actionFrame.qleHouseName.text()) ]
      if data[0]!="" and data[1]!="" and data[2]!="":
        if self.checkMember(data[0]) == False :
          query = "INSERT INTO MemberList(membershipno,Name,House) VALUES(%s,%s,%s)"
          c = self.execute_query( query, data )
          data1 = self.get_family_members()
          if len(data1)>0:
            for f in data1:
              data2 = ( data[0], f , )
              query = "INSERT INTO family(membershipno,fname) VALUES(%s,%s)"
              self.execute_query( query, data2 )
          if c != False:
            self.clearNewMemberForm()
            self.showMessageBox("Member added")
            self.actionFrame.qleBookNumber.setStyleSheet("font:bold 18pt Ubuntu;")
          else:
            self.showMessageBox("Error occurred")
            self.actionFrame.qleBookNumber.setFocus()
            self.actionFrame.qleBookNumber.selectAll()
        else:
          self.showMessageBox("Membership number already exists",2)
          self.actionFrame.qleBookNumber.setFocus()
          self.actionFrame.qleBookNumber.selectAll()
          self.actionFrame.qleBookNumber.setStyleSheet("color:red;background-color:yellow;font:bold 18pt Ubuntu;")
      else:
        self.showMessageBox("Fill all fields",2)
      
    def checkMember(self, membershipno):
      data = (membershipno,)
      query = "SELECT * FROM MemberList WHERE membershipno=%s"
      c = self.execute_query( query, data )
      if  c != False:
        result = c.fetchone()
        if result:
          return True
        else:
          return False
      else:
        return False

    def showMemberList(self):
      from member_list import Member_List      
      self.showActions( Member_List() )
      
      self.actionFrame.tbl_member_list.setFocus()
      
      self.signal_connect( self.actionFrame.btnViewMember, self.viewMember )
      self.signal_connect( self.actionFrame.btnEditMember, self.editMember )
      self.signal_connect( self.actionFrame.btnDeleteMember, self.deleteMember )
      self.signal_connect( self.actionFrame.btnPrint, self.printReport )
      
      self.showBack(self.showMemberFunctions)
      
      width = self.width() - 394
      
      self.actionFrame.tbl_member_list.setColumnWidth(0,140)
      self.actionFrame.tbl_member_list.setColumnWidth(1,width)
      self.actionFrame.tbl_member_list.setColumnWidth(2,120)
      
      
      query = """SELECT m.membershipno,name,SUM(amount) AS amount 
              FROM MemberList m 
              LEFT JOIN collection c ON(c.membershipno=m.membershipno) 
              GROUP BY m.membershipno,m.name
              ORDER BY membershipno """
      c = self.execute_query( query ) 
      if c != False:
        count=0
        for d in c.fetchall():
          self.actionFrame.tbl_member_list.setRowCount(count+1)
          
          self.actionFrame.tbl_member_list.setItem(count,0,QtGui.QTableWidgetItem(QtCore.QString.number(d[0])))
          self.actionFrame.tbl_member_list.setItem(count,1,QtGui.QTableWidgetItem(d[1]))
          
          if d[2]:
            self.actionFrame.tbl_member_list.setItem(count,2,QtGui.QTableWidgetItem(QtCore.QString.number(d[2])))
          else:
            self.actionFrame.tbl_member_list.setItem(count,2,QtGui.QTableWidgetItem("0"))
          
          total = current_week * 10
          exp = total - 80
          
          if int( self.actionFrame.tbl_member_list.item(count,2).text() ) < exp:
            self.actionFrame.tbl_member_list.item(count,0).setForeground(QtGui.QBrush(QtGui.QColor("red") ) )
            self.actionFrame.tbl_member_list.item(count,1).setForeground(QtGui.QBrush(QtGui.QColor("red") ) )
            self.actionFrame.tbl_member_list.item(count,2).setForeground(QtGui.QBrush(QtGui.QColor("red") ) )
          
          self.actionFrame.tbl_member_list.item(count,0).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_member_list.item(count,2).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          
          self.actionFrame.tbl_member_list.setRowHeight(count,50)
          count+=1
        
        if count>0:
          self.actionFrame.tbl_member_list.setCurrentItem( self.actionFrame.tbl_member_list.item(0,0) )
    
    def editMember(self):
      row = self.actionFrame.tbl_member_list.currentRow()
      if row > -1:
        membershipno = self.actionFrame.tbl_member_list.item(row,0).text()
        from member_edit import Member_Edit
        self.showActions( Member_Edit() )
        self.showBack(self.showMemberList)
        self.actionFrame.qleMemberName.setFocus()
        data = [ str( membershipno ), ]
        query = "SELECT * FROM MemberList WHERE membershipno=%s"
        c = self.execute_query( query, data )
        if c != False:
          result = c.fetchone()
          self.actionFrame.qleBookNumber.setText( QtCore.QString.number(result[0] ) )
          self.actionFrame.qleMemberName.setText( result[1] )
          self.actionFrame.qleHouseName.setText( result[2] )
          self.signal_connect( self.actionFrame.btnUpdate, self.updateMember )
          
          query = "SELECT * FROM family WHERE membershipno=%s"
          c = self.execute_query( query, data )
          if c != False:
            family = []
            for f in c.fetchall():
              family.append( f[1] )
            if family[0]!="":
              self.actionFrame.qle1.setText(family[0])
            if family[1]!="":
              self.actionFrame.qle2.setText(family[1])
            if family[2]!="":
              self.actionFrame.qle3.setText(family[2])
            if family[3]!="":
              self.actionFrame.qle4.setText(family[3])
            if family[4]!="":
              self.actionFrame.qle5.setText(family[4])
            if family[5]!="":
              self.actionFrame.qle6.setText(family[5])
    
    def updateMember(self):
      data = [ str(self.actionFrame.qleMemberName.text()),str(self.actionFrame.qleHouseName.text()), str(self.actionFrame.qleBookNumber.text()) ]
      if data[0]!="" and data[1]!="" and data[2]!="":
          query = "UPDATE MemberList SET Name=%s, House=%s WHERE membershipno=%s"
          c = self.execute_query( query, data )
          data1 = self.get_family_members()
          data3 = [ data[2], ]
          query = "DELETE FROM family WHERE membershipno=%s"
          self.execute_query( query, data3 )
          if len(data1)>0:
            for f in data1:
              data2 = ( data[2], f , )
              query = "INSERT INTO family(membershipno,fname) VALUES(%s,%s)"
              self.execute_query( query, data2 )
          if c != False:
            self.clearNewMemberForm()
            self.showMessageBox("Member updated")
            self.showMemberList()
          else:
            self.showMessageBox("Error occurred")
            self.actionFrame.qleBookNumber.setFocus()
            self.actionFrame.qleBookNumber.selectAll()
      else:
        self.showMessageBox("Fill all fields",2)
        
    def deleteMember(self):
      row = self.actionFrame.tbl_member_list.currentRow()
      if row > -1:
        membershipno = self.actionFrame.tbl_member_list.item(row,0).text()
        r = QtGui.QMessageBox.warning(self,"Delete member?","Delete member?", QtGui.QMessageBox.Yes|QtGui.QMessageBox.Default, QtGui.QMessageBox.No|QtGui.QMessageBox.Escape)
        if r == QtGui.QMessageBox.Yes:
          if self.checkMemberCollection( membershipno ):
            data = ( str( membershipno ), )
            query = "DELETE FROM MemberList WHERE membershipno=%s"
            self.execute_query( query, data )
            self.showMemberList()
            self.showMessageBox("Member deleted")
    
    def getExpenseIncome(self):
      balance = 0
      query1 = "SELECT COUNT(*) FROM MemberList"
      query2 = """SELECT COUNT(*) FROM rip"""
      c1 = self.execute_query(query1)
      c2 = self.execute_query(query2)
      if c1!=False and c2 != False:
        deaths = c2.fetchone()
        members = c1.fetchone()
        if deaths[0]:
           death_total_exp = deaths[0]*5000
        else:
           death_total_exp = 0
        if members[0]:
          exp = members[0] * 50
          members_count = members[0]
        else:
          exp = 0
          members_count = members[0]
        balance = ( death_total_exp + exp ) / members_count
        return balance
      
    def printReport(self):
      D = []
      query = """SELECT m.membershipno,name,SUM(amount) AS amount 
              FROM MemberList m 
              LEFT JOIN collection c ON(c.membershipno=m.membershipno) 
              GROUP BY m.membershipno,m.name
              ORDER BY membershipno """
      c = self.execute_query( query )
      for x in c.fetchall():
          D.append(x)
      abc=[]
      abc.append(['Sl', 'Name','Amount','Balance'])
      bal = self.getExpenseIncome()
      for x in D:
          if x[2]:
            if x[2]<440:
              balance=0
            else:
              balance=x[2]-452.63
          else:
            balance=0
          abc.append([x[0],strip(upper(x[1])),x[2],balance])
      
      c =self.execute_query("""SELECT SUM(amount) FROM collection""")
      DAT = c.fetchone()
      total=str(DAT[0])
      
      elements = []
      styles = getSampleStyleSheet()
      pdf_filename = 'HMCC_' + startyear + "_" + endyear + '.pdf'
      doc = SimpleDocTemplate( '/tmp/' + pdf_filename, pagesize=A4,topMargin=2,bottomMargin=2)
      elements.append(Paragraph("HMCC Report", styles['Heading1']))
      strDate=datetime.datetime.today().strftime("Date: %Y-%m-%d Time: %H:%M:%S")
      elements.append(Paragraph(strDate, styles['Heading3']))
      elements.append(Paragraph("Total: Rs." + total, styles['Heading3']))
      ts = []
      table = Table(abc, style=ts)
      table.hAlign = 'LEFT'
      elements.append(table)
      doc.build(elements)
      #strDate1 = datetime.datetime.today().strftime("%Y-%m-%d_%H-%M-%S")
      strCP="cp /tmp/" + pdf_filename + " ~/Desktop/" + pdf_filename
      os.system( strCP )
      self.showMessageBox("Done. Check Desktop")
      
    def checkMemberCollection(self, membershipno):
      data = ( str( membershipno ), )
      query = "SELECT 1 FROM collection WHERE membershipno=%s"
      c = self.execute_query( query, data )
      if c != False:
        result = c.fetchone()
        if result:
          self.showMessageBox("Cannot delete member. Active member", 2 )
          return False
        else:
          return True
          
    def viewMember(self):
      row = self.actionFrame.tbl_member_list.currentRow()
      if row > -1:
        membershipno = self.actionFrame.tbl_member_list.item(row,0).text()
        from member_view import Member_View
        self.showActions( Member_View() )
        self.actionFrame.tbl_Family.setColumnWidth(0,300)
        self.showBack( self.showMemberList )
        data = ( str( membershipno ), )
        query = """SELECT m.membershipno,name,SUM(amount) AS amount, house 
              FROM MemberList m 
              LEFT JOIN collection c ON(c.membershipno=m.membershipno) 
              WHERE m.membershipno=%s
              GROUP BY m.membershipno,m.name,m.house
              ORDER BY m.membershipno """
        c = self.execute_query( query, data )
        if c != False:
          result = c.fetchone()
          self.actionFrame.lblBookNumber.setText( "Book number: " + QtCore.QString.number( result[0] ) )
          self.actionFrame.lblMemberName.setText( "Member: " + result[1] )
          if result[2]:
            self.actionFrame.lblAmount.setText( "Amount: " + QtCore.QString.number( result[2] ) )
          else:
            self.actionFrame.lblAmount.setText( "Amount: 0" )
          self.actionFrame.lblHouseName.setText( "House Name: " + result[3] )
          self.showCollectionHistory1( membershipno )
          
          query = "SELECT * FROM family WHERE membershipno=%s"
          c = self.execute_query( query, data )
          if c!= False:
            count=0
            for d in c.fetchall():
              self.actionFrame.tbl_Family.setRowCount(count+1)
              self.actionFrame.tbl_Family.setItem(count,0,QtGui.QTableWidgetItem(d[1]))
              self.actionFrame.tbl_Family.setRowHeight(count,50)
              count+=1
    
    def showCollectionHistory1(self, membershipno ):
      count = 0
      self.actionFrame.tbl_Collection_History.clearContents()
      self.actionFrame.tbl_Collection_History.setRowCount(count)
      self.actionFrame.tbl_Collection_History.setColumnWidth(0,140)
      data = [ str( membershipno), ]
      query = """SELECT date,weekno,receiptno,amount FROM collection WHERE membershipno = %s 
                  ORDER BY weekno"""
      c = self.execute_query( query, data )
      if c != False:
        for d in c.fetchall():
          self.actionFrame.tbl_Collection_History.setRowCount(count+1)
          dt = datetime.datetime.strptime(d[0],"%Y-%m-%d").strftime("%d-%m-%Y")
          self.actionFrame.tbl_Collection_History.setItem(count,0,QtGui.QTableWidgetItem(dt))
          self.actionFrame.tbl_Collection_History.setItem(count,1,QtGui.QTableWidgetItem(QtCore.QString.number(d[1])))
          self.actionFrame.tbl_Collection_History.setItem(count,2,QtGui.QTableWidgetItem(QtCore.QString.number(d[2])))
          self.actionFrame.tbl_Collection_History.setItem(count,3,QtGui.QTableWidgetItem(QtCore.QString.number(d[3])))
          
          self.actionFrame.tbl_Collection_History.item(count,0).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.setRowHeight(count,50)
          
          self.actionFrame.tbl_Collection_History.item(count,0).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.item(count,1).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.item(count,2).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.item(count,3).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.setRowHeight(count,50)
          
          count+=1
      
    def showFamily(self):
      from member_family import Family_Members
      self.showActions( Family_Members() )
      self.showBack(self.showMemberFunctions)
      self.actionFrame.qle1.setFocus()

######################### End of member functions ##############

######################### Collection ###########################
    def showCollectionFunctions(self):
      from collection_actions import Collection_Actions
      self.showActions( Collection_Actions() )
      self.showBack(self.clear_Layout)
      
      self.signal_connect(self.actionFrame.btnCollectionEntry,self.showCollectionEntryForm)
      self.signal_connect(self.actionFrame.btnCollectionList,self.showCollectionList)
      self.signal_connect(self.actionFrame.btnIncome,self.showIncomeExpense)
    
    def showIncomeExpense(self):
      from income_expense import Income_Expense
      self.showActions(Income_Expense())
      self.showBack(self.showCollectionFunctions)
      reg = QtCore.QRegExp( "^[0-9]*$" )
      validator = QtGui.QRegExpValidator(reg, self);
      self.actionFrame.qleDate.setFocus()
      self.actionFrame.qleDate.setDate(today)
      self.actionFrame.qleAmount.setValidator( validator )
      self.signal_connect(self.actionFrame.btnSubmit, self.addIncomeExpense)
      self.signal_connect(self.actionFrame.btnInExpEntries,self.showIncomeExpenseEntries)
      
    def deleteIncomeExpense(self):
      r = QtGui.QMessageBox.warning(self,"Income Expense","Delete entry?", QtGui.QMessageBox.Yes|QtGui.QMessageBox.Default, QtGui.QMessageBox.No|QtGui.QMessageBox.Escape)
      if r == QtGui.QMessageBox.Yes:
        query = "DELETE FROM income_expense WHERE id=%s"
        row = self.actionFrame.tbl_InExp_entries.currentRow()
        id = str( self.actionFrame.tbl_InExp_entries.item(row,0).text() )
        data3 = [ id, ]
        self.execute_query( query, data3 )
        self.showIncomeExpenseEntries()
      
    def showIncomeExpenseEntries(self):
      from income_expense_entries import Income_Expense_Entries
      self.showActions(Income_Expense_Entries())
      self.showBack(self.showIncomeExpense)
      
      self.signal_connect(self.actionFrame.btnDelete, self.deleteIncomeExpense)
      
      query = """SELECT a.id,a.date,a.description,a.debit,a.credit 
              FROM income_expense a
              ORDER BY date """
      c = self.execute_query( query ) 
      if c != False:
        count=0
        income=0
        expense=0
        for d in c.fetchall():
          self.actionFrame.tbl_InExp_entries.setRowCount(count+1)
          self.actionFrame.tbl_InExp_entries.setItem(count,0,QtGui.QTableWidgetItem(QtCore.QString.number(d[0])))
          self.actionFrame.tbl_InExp_entries.setItem(count,1,QtGui.QTableWidgetItem(QtCore.QString.number(count+1)))
          dt = datetime.datetime.strptime(d[1],"%Y-%m-%d").strftime("%d-%m-%Y")
          self.actionFrame.tbl_InExp_entries.setItem(count,2,QtGui.QTableWidgetItem(dt))
          self.actionFrame.tbl_InExp_entries.setItem(count,3,QtGui.QTableWidgetItem(d[2]))
          
          if d[3]:
            self.actionFrame.tbl_InExp_entries.setItem(count,4,QtGui.QTableWidgetItem(QtCore.QString.number(d[3])))
            income+=d[3]
          
          if d[4]:
            self.actionFrame.tbl_InExp_entries.setItem(count,5,QtGui.QTableWidgetItem(QtCore.QString.number(d[4])))
            expense+=d[4]
            
          self.actionFrame.lblIncome.setText("Income: " + QtCore.QString.number(income) )
          self.actionFrame.lblExpense.setText("Expense: " + QtCore.QString.number(expense) )
          
          self.actionFrame.tbl_InExp_entries.item(count,1).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          
          self.actionFrame.tbl_InExp_entries.setColumnWidth(0,0)
          self.actionFrame.tbl_InExp_entries.setColumnWidth(1,100)
          self.actionFrame.tbl_InExp_entries.setColumnWidth(2,300)
          self.actionFrame.tbl_InExp_entries.setColumnWidth(3,100)
          self.actionFrame.tbl_InExp_entries.setColumnWidth(4,100)
          
          self.actionFrame.tbl_InExp_entries.setRowHeight(count,50)
          count+=1
        
        if count>0:
          self.actionFrame.tbl_InExp_entries.setCurrentItem( self.actionFrame.tbl_InExp_entries.item(0,0) )
      
      self.actionFrame.tbl_InExp_entries.setColumnWidth(1,120)
      self.actionFrame.tbl_InExp_entries.setColumnWidth(2,400)
    
    def addIncomeExpense(self):
      date = self.actionFrame.qleDate.date().toString(QtCore.Qt.ISODate)
      data = ( str(self.actionFrame.qleAmount.text()), str(date), str(self.actionFrame.pteDesc.toPlainText()), )
      if data[0]!="" and data[2]!="":
        if self.actionFrame.cmbCat.currentText() == "Income":
          query = "INSERT INTO income_expense(debit,date,description) VALUES(%s,%s,%s)"
        else:
          query = "INSERT INTO income_expense(credit,date,description) VALUES(%s,%s,%s)"
        
        c=self.execute_query(query,data)
        
        if c!=False:
          self.actionFrame.qleDate.setFocus()
          self.actionFrame.qleAmount.setText("")
          self.actionFrame.pteDesc.setPlainText("")
          self.showMessageBox("Entry added successfully")
      else:
        self.showMessageBox("Enter amount and description",2)
      
    def showCollectionEntryForm(self):
      if enddate_payment < datetime.datetime.now().date() or current_week<1 :
        QtGui.QMessageBox.warning(self,"Error","Cannot accept payment")
        return
      from collection_new import Collection_New
      self.showActions( Collection_New() )
      self.actionFrame.qleBookNumber.setFocus()
      
      self.connect(self.actionFrame.qleBookNumber,QtCore.SIGNAL('returnPressed()'),self.activate_collection_amount)
      self.connect(self.actionFrame.qleAmount,QtCore.SIGNAL('returnPressed()'),self.acceptPayment)
      
      reg = QtCore.QRegExp( "^[0-9]*$" )
      validator = QtGui.QRegExpValidator(reg, self);
      self.actionFrame.qleBookNumber.setValidator( validator )
      self.actionFrame.qleAmount.setValidator( validator )
      
      self.signal_connect(self.actionFrame.btnOk,self.activate_collection_amount)
      self.signal_connect(self.actionFrame.btnClear,self.clearCollectionForm)
      self.signal_connect(self.actionFrame.btnAccept,self.acceptPayment)
      
      self.showBack(self.showCollectionFunctions)
      
      self.updateTodayTotal()
      
    def activate_collection_amount(self):
      self.actionFrame.qleReceiptNo.setText("")
      if self.actionFrame.qleBookNumber.text() != "":
        membershipno = str( self.actionFrame.qleBookNumber.text() )
        if self.checkMember( membershipno ):
          self.collection_show_member()
          if self.check_due( membershipno ):
            if self.check_payment( str( self.actionFrame.qleBookNumber.text() ) ):
              self.showMessageBox("Payment already received",2)
              self.actionFrame.qleBookNumber.setFocus()
              self.actionFrame.qleBookNumber.selectAll()
            else:
              self.hideMessageBox()
              self.actionFrame.qleBookNumber.setEnabled(False)
              self.actionFrame.qleAmount.setEnabled(True)
              self.actionFrame.qleAmount.setFocus()
              self.actionFrame.qleBookNumber.setStyleSheet("font:bold 18pt Ubuntu;")
          else:
            self.showMessageBox("Member due for 8 weeks",2)
            self.actionFrame.qleBookNumber.selectAll()
            
        else:
          self.collection_show_member(1)
          self.actionFrame.qleBookNumber.setStyleSheet("color:red;background-color:yellow;font:bold 18pt Ubuntu;")
          self.showMessageBox("Member does not exists",2)
          self.actionFrame.lblMemberName.setText( "" )
          self.actionFrame.lblBookNumber.setText( "" )
          self.actionFrame.lblAmount.setText( "" )
          self.actionFrame.qleBookNumber.setFocus()
          self.actionFrame.qleBookNumber.selectAll()
          
      else:
        self.showMessageBox("Enter the book number",2)
        self.actionFrame.qleBookNumber.setFocus()
        self.actionFrame.qleBookNumber.setStyleSheet("color:red;background-color:yellow;font:bold 18pt Ubuntu;")
    
    def clearCollectionForm(self):
      self.actionFrame.qleBookNumber.setEnabled(True)
      self.actionFrame.qleAmount.setEnabled(False)
      self.actionFrame.qleAmount.setText("")
      self.actionFrame.lblAmount.setText( "" )
      self.actionFrame.qleBookNumber.setText("")
      self.actionFrame.qleBookNumber.setFocus()
      self.actionFrame.tbl_Collection_History.clearContents()
      self.actionFrame.tbl_Collection_History.setRowCount(0)
    
    def collection_show_member(self,clear=0):
      if clear==0:
        membershipno = str( self.actionFrame.qleBookNumber.text() )
        data = [ membershipno ]
        query = """SELECT name,SUM(amount) FROM MemberList m
                LEFT JOIN collection c ON(c.membershipno=m.membershipno) 
                WHERE m.membershipno=%s GROUP BY m.name"""
        c = self.execute_query( query, data )
        if c!= False:
          result = c.fetchone()
          self.actionFrame.lblMemberName.setText( result[0] )
          self.actionFrame.lblBookNumber.setText( membershipno )
          if result[1]:
            self.actionFrame.lblAmount.setText( "Total Amount: " + QtCore.QString.number( result[1] ) )
          self.showCollectionHistory( membershipno )
      else:
        self.clearCollectionForm()
          
    def showCollectionHistory(self, membershipno ):
      count = 0
      self.actionFrame.tbl_Collection_History.clearContents()
      self.actionFrame.tbl_Collection_History.setRowCount(count)
      self.actionFrame.tbl_Collection_History.setColumnWidth(0,140)
      data = [ str( membershipno), ]
      query = "SELECT date,weekno,receiptno,amount FROM collection WHERE membershipno = %s ORDER BY receiptno DESC"
      c = self.execute_query( query, data )
      if c != False:
        for d in c.fetchall():
          self.actionFrame.tbl_Collection_History.setRowCount(count+1)
          dt = datetime.datetime.strptime(d[0],"%Y-%m-%d").strftime("%d-%m-%Y")
          self.actionFrame.tbl_Collection_History.setItem(count,0,QtGui.QTableWidgetItem(dt))
          self.actionFrame.tbl_Collection_History.setItem(count,1,QtGui.QTableWidgetItem(QtCore.QString.number(d[1])))
          self.actionFrame.tbl_Collection_History.setItem(count,2,QtGui.QTableWidgetItem(QtCore.QString.number(d[2])))
          self.actionFrame.tbl_Collection_History.setItem(count,3,QtGui.QTableWidgetItem(QtCore.QString.number(d[3])))
          
          self.actionFrame.tbl_Collection_History.item(count,0).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.setRowHeight(count,50)
          
          self.actionFrame.tbl_Collection_History.item(count,0).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.item(count,1).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.item(count,2).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.item(count,3).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_Collection_History.setRowHeight(count,50)
          
          count+=1
          
        if count>0:
          self.actionFrame.tbl_Collection_History.setCurrentItem( self.actionFrame.tbl_Collection_History.item(0,0) )
          
    def acceptPayment(self):
      if self.actionFrame.qleAmount.text() != "":
        data = [ str( self.actionFrame.qleBookNumber.text() ), str( self.actionFrame.qleAmount.text() ), current_week, today_db  ]
        if self.check_amount( data[1] ):
          query = "INSERT INTO collection (MemberShipNo,  Amount, weekno, Date) VALUES(%s, %s, %s, %s)"
          self.execute_query( query, data )
          self.hideMessageBox()
          self.showMessageBox("Payment received")
          self.updateTodayTotal()
          receiptno = self.showReceiptNo()
          
          #r = QtGui.QMessageBox.warning(self,"Print receipt?","Print receipt?", QtGui.QMessageBox.Yes|QtGui.QMessageBox.Default, QtGui.QMessageBox.No|QtGui.QMessageBox.Escape)
          #if r == QtGui.QMessageBox.Yes:
          #  os.system("./reverse.sh")
           
          #  lp = open("/dev/usb/lp0", "w")
          #  lp.write("\n\n\n")
          #  lp.write("\tHMCC - Mattancherry\n")
          #  lp.write("\t       Receipt\n")
          #  lp.write("\t--------------------\n")
          #  lp.write("\tReceipt No   : " + str( receiptno ) + "\n")
          #  lp.write("\tDate         : " + today_str + "\n")
          #  lp.write("\tWeek         : " + str(current_week) + "\n")
          #  lp.write("\tMembership No: " + data[0] + "\n")
          #  lp.write("\tAmount       : " + data[1] + "\n")
          #  lp.write("\n\n\t*** Thank you ***\n")
          #  for x in range(0, 20):
          #    lp.write("\n")
          #  lp.close()
          
          self.actionFrame.qleAmount.setStyleSheet("font:bold 18pt Ubuntu;")
          self.clearCollectionForm()
        else:
          self.actionFrame.qleAmount.setStyleSheet("color:red;background-color:yellow;font:bold 18pt Ubuntu;")
          self.showMessageBox("Collection amount should be 10/20/30/40",2)
          self.actionFrame.qleAmount.setFocus()
          self.actionFrame.qleAmount.selectAll()
      else:
        self.showMessageBox("Enter collection amount",2)        

    def showReceiptNo(self):
        data = ( current_week, )
        query = "SELECT MAX(receiptno) FROM collection WHERE weekno=%s"
        c = self.execute_query(query, data)
        if c != False:
          result = c.fetchone()
          if result[0]:
            self.actionFrame.qleReceiptNo.setText( QtCore.QString.number( result[0] ) )
            return result[0]
          else:
            self.actionFrame.qleReceiptNo.setText( QtCore.QString.number( 0 ) )
            return 0
      
    def check_amount(self, amount):
      amounts = { 10: True,
                  20: True,
                  30: True,
                  40: True
                }
      if amounts.get(int(amount)):
        return True
      else:
        return False
        
    def check_payment(self,membershipno, receiptno=0):
      data = [ membershipno, current_week ]
      if receiptno==0:
        query = "SELECT amount, receiptno FROM collection WHERE membershipno = %s AND weekno = %s"
      else:
        data = [ membershipno, current_week, receiptno ]
        query = "SELECT amount, receiptno FROM collection WHERE membershipno = %s AND weekno = %s AND receiptno!=%s"
      c = self.execute_query( query, data )
      result = c.fetchone()
      if result:
        return True
      else:
        return False
    
    def check_due(self, membershipno):
      total = current_week * 10
      exp = total - 80
      data = ( membershipno, )
      query = """SELECT SUM(amount),COUNT(1)
              FROM collection WHERE membershipno=%s"""
      c= self.execute_query( query, data )
      if c != False:
        result = c.fetchone()
        if result[0] >= exp or result[1]>=(current_week-8):
          return True
        else:
          return False
      return True
      
    def updateTodayTotal(self):
      data = ( current_week, )
      query = "SELECT SUM(amount) FROM collection WHERE weekno=%s"
      
      c = self.execute_query(query, data)
      if c != False:
        result = c.fetchone()
        if result[0]:
          self.actionFrame.qleWeekTotalCollection.setText( QtCore.QString.number( result[0] ) )
        else:
          self.actionFrame.qleWeekTotalCollection.setText( QtCore.QString.number( 0 ) )
      
    def showCollectionList(self):
      from collection_list import Collection_List
      self.showActions( Collection_List() )
      self.showBack(self.showCollectionFunctions)
      
      self.signal_connect(self.actionFrame.btnEdit, self.showEditCollection)
      
      self.actionFrame.tbl_collection_list.setColumnWidth(0,140)
      self.actionFrame.tbl_collection_list.setColumnWidth(1,140)
      self.actionFrame.tbl_collection_list.setColumnWidth(2,300)
      self.actionFrame.tbl_collection_list.setColumnWidth(4,200)
      
      reg = QtCore.QRegExp( "^[0-9]*$" )
      validator = QtGui.QRegExpValidator(reg, self);
      self.actionFrame.qleWeekNo.setValidator( validator )
      
      self.connect(self.actionFrame.qleWeekNo,QtCore.SIGNAL('returnPressed()'),self.updateCollectionList)
      
      self.actionFrame.qleWeekNo.setText(QtCore.QString.number(current_week))
      
      self.actionFrame.qleWeekNo.setFocus()
      
      self.signal_connect(self.actionFrame.btnOk,self.updateCollectionList)
      self.signal_connect(self.actionFrame.btnToday,self.showTodayCollectionList)
      
      self.updateCollectionList()
    
    def showEditCollection(self):
      row = self.actionFrame.tbl_collection_list.currentRow()
      if row > -1:
        receiptno = self.actionFrame.tbl_collection_list.item(row,0).text()
        membershipno = self.actionFrame.tbl_collection_list.item(row,1).text()
        from collection_edit import Collection_Edit
        self.showActions(Collection_Edit())
        self.showBack(self.showCollectionList)
        self.signal_connect(self.actionFrame.btnUpdate, self.updateCollection )
        reg = QtCore.QRegExp( "^[0-9]*$" )
        validator = QtGui.QRegExpValidator(reg, self);
        self.actionFrame.qleAmount.setValidator( validator )
        self.actionFrame.qleBookNumber.setValidator( validator )
        self.actionFrame.qleReceiptNo.setText( receiptno )
        self.actionFrame.qleBookNumber.setText( membershipno )
        self.actionFrame.qleAmount.setFocus()
        data = [ str(receiptno), ]
        query = "SELECT amount FROM collection WHERE receiptno=%s"
        c = self.execute_query( query, data )
        if c!=False:
          result = c.fetchone()
          if result:
            self.actionFrame.qleAmount.setText( QtCore.QString.number( result[0] ) )
    
    def updateCollection(self):
      data = [ str(self.actionFrame.qleAmount.text() ), str(self.actionFrame.qleBookNumber.text() ), str(self.actionFrame.qleReceiptNo.text()) ]
      if data[0] != "":
        if self.check_amount( data[0] ):
          if data[1] != "":
            if self.checkMember( data[1] ):
              if self.check_payment(data[1],data[2] ):
                self.showMessageBox("Payment already done",2)
              else:
                query = "UPDATE collection SET amount=%s,membershipno=%s WHERE receiptno=%s"
                self.execute_query( query, data )
                
                data = ( data[2], )
                query = """SELECT * FROM collection WHERE receiptno=%s"""
                c = self.execute_query( query, data )
                if c!= False:
                  result = c.fetchone()
                  today_str1 = datetime.datetime.now().strftime("%Y-%m-%d")
                  data = ( today_str1, result[0], result[1], result[2], result[3], result[4], )
                  query = """INSERT INTO collection_bak(modified_date,
                          membershipno,date,amount,receiptno,weekno) 
                          VALUES(%s,%s,%s,%s,%s,%s)"""
                  self.execute_query( query, data )
                self.showCollectionList()
                self.showMessageBox("Collection updated")
            else:
              self.showMessageBox("Member does not exists",2)
          else:
            self.showMessageBox("Enter book number",2)
        else:
          self.showMessageBox("Collection amount should be 10/20/30/40",2) 
      else:
        self.showMessageBox("Enter amount",2)
      
    def showTodayCollectionList(self):
      self.actionFrame.qleWeekNo.setText(QtCore.QString.number(current_week))
      self.updateCollectionList()
      
    def updateCollectionList(self):
      self.actionFrame.qleWeekNo.selectAll()
      self.actionFrame.qleWeekNo.setFocus()
      query = """SELECT receiptno,m.membershipno,name,amount,date 
              FROM MemberList m 
              LEFT JOIN collection c ON(c.membershipno=m.membershipno) 
              WHERE weekno=%s 
              GROUP BY receiptno,c.date,m.membershipno,m.name,amount
              ORDER BY receiptno DESC"""
      week = self.actionFrame.qleWeekNo.text()
      data = ( str(week), )
      
      query1 = """SELECT SUM(amount) FROM collection WHERE weekno=%s"""
      c = self.execute_query( query1, data )
      d = c.fetchone()
      if d[0]:
        self.actionFrame.lblWeeklyTotal.setText("Total: " + QtCore.QString.number(d[0]))
      else:
        self.actionFrame.lblWeeklyTotal.setText("Total: 0")
      
      c = self.execute_query( query, data )
      if c!=False:
        count=0
        for d in c.fetchall():
          self.actionFrame.tbl_collection_list.setRowCount(count+1)
          self.actionFrame.tbl_collection_list.setItem(count,0,QtGui.QTableWidgetItem(QtCore.QString.number(d[0])))
          self.actionFrame.tbl_collection_list.setItem(count,1,QtGui.QTableWidgetItem(QtCore.QString.number(d[1])))
          self.actionFrame.tbl_collection_list.setItem(count,2,QtGui.QTableWidgetItem(d[2]))
          self.actionFrame.tbl_collection_list.setItem(count,3,QtGui.QTableWidgetItem(QtCore.QString.number(d[3])))
          dt = datetime.datetime.strptime(d[4],"%Y-%m-%d").strftime("%d-%m-%Y")
          self.actionFrame.tbl_collection_list.setItem(count,4,QtGui.QTableWidgetItem(dt))
          
          self.actionFrame.tbl_collection_list.item(count,0).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_collection_list.setRowHeight(count,50)
          
          self.actionFrame.tbl_collection_list.item(count,0).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_collection_list.item(count,1).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_collection_list.item(count,3).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_collection_list.item(count,4).setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
          self.actionFrame.tbl_collection_list.setRowHeight(count,50)
          
          count+=1
      
        if count==0:      
          self.actionFrame.tbl_collection_list.clearContents()
          self.actionFrame.tbl_collection_list.setRowCount(0)
          QtGui.QMessageBox.information(self,"No data", "0 records found")
      
#########################End of Collection functions ###########

######################### Report ###############################
    def showReportFunctions(self):
      from ui.reports_ui import Ui_Reports_Actions
      self.showActions( Ui_Reports_Actions() )
      self.showBack(self.clear_Layout)
######################### End of Report functions ##############

######################### Admin functions ######################
    def showAdminFunctions(self):
      from admin_actions import Admin_Actions
      self.showActions( Admin_Actions() )
      self.showBack(self.clear_Layout)
      self.signal_connect(self.actionFrame.btnYearEnd,self.newFiscal)
      self.signal_connect(self.actionFrame.btnBackup,self.backup)
	
    def backup(self):
      r = QtGui.QMessageBox.warning(self,"Backup","Proceed?", QtGui.QMessageBox.Yes|QtGui.QMessageBox.Default, QtGui.QMessageBox.No|QtGui.QMessageBox.Escape)
      if r == QtGui.QMessageBox.Yes:
        filename = "~/%s.sql" % (db_name)
        pg_dump = "pg_dump -U postgres %s -f %s" % (db_name,filename)
        os.system(pg_dump)
      
    def newFiscal(self):
      if today >= enddate_payment:
        self.showMessageBox("Please wait...")
        self.actionFrame.setEnabled(False)
        r =  QtGui.QInputDialog.getText(self, "Confirm",
                                    "Enter code",QtGui.QLineEdit.Password)
        if r[1]==True:
          if str(r[0]) == "1":
            startdate = enddate + datetime.timedelta(7)
            startyear = startdate.strftime("%Y")
            end_date = startdate + datetime.timedelta(51 * 7)
            endyear = end_date.strftime("%Y")
            
            new_db_name = "hmcc_" + startyear + "-" + endyear
            c= db.cursor()
            query = 'CREATE DATABASE "%s"' % new_db_name
            try:
              c.execute("commit")
              c.execute( query )
            except Exception, e:
              print e
              db.rollback()
              pass
            else:
              tables = "-t memberlist -t family "
              filename = "/tmp/hmcc_new_fiscal.sql"
              pg_dump = "pg_dump -U postgres " + tables + "%s -f %s" % (db_name,filename)
              os.system(pg_dump)
              pg_restore = "psql -U postgres %s -f %s" % (new_db_name,filename)
              os.system(pg_restore)
              
              tables = "-t collection -t collection_bak -t rip -t income_expense -t rcptno -s "
              pg_dump = "pg_dump -U postgres " + tables + "%s -f %s" % (db_name,filename)
              os.system(pg_dump)
              pg_restore = "psql -U postgres %s -f %s" % (new_db_name,filename)
              os.system(pg_restore)
              
              Config = ConfigParser.ConfigParser()
              Config.read("settings.ini")
              cfgfile = open("settings.ini",'w')
              Config.set('General','startdate', startdate.strftime("%d-%m-%Y") )
              Config.set('General','enddate', end_date.strftime("%d-%m-%Y") )
              Config.write(cfgfile)
              cfgfile.close()
              QtGui.QMessageBox.information(self,"Restart", "Application will close now. Please open it again")
              exit(0)
          else:
            QtGui.QMessageBox.warning(self,"Invalid","Invalid code")
      else:
        QtGui.QMessageBox.critical(self,"Error","Thalennu cheythal mathi")
      self.hideMessageBox()
      self.actionFrame.setEnabled(True)

######################### End of Admin functions ###############


if __name__ == "__main__":
  
  #change process name
  procname.setprocname("hmcc")
  
  app = QtGui.QApplication(sys.argv)
  
  #list of QT Ui styles
  styles = [ "Bespin", "Oxygen", "Cleanlooks", "GTK+", "Plastique", "Polyester", "QtCurve" ]
  
  #parse settings.ini to get the last activated style id
  Config = ConfigParser.ConfigParser()
  Config.read("settings.ini")
  style = int( Config.get("General", "style" ) )
  
  date_format = "%d-%m-%Y"
  
  startdate_str = Config.get("General", "startdate" )
  startdate = datetime.datetime.strptime(startdate_str,date_format).date()
  startyear = startdate.strftime("%Y")
  
  enddate_str = Config.get("General", "enddate" )
  enddate = datetime.datetime.strptime(enddate_str,date_format).date()
  endyear = enddate.strftime("%Y")
  
  #last sunday + 6 days
  enddate_payment = enddate + datetime.timedelta(6)
  
  week = findWeek( startdate_str )
  current_week = week[0]
  
  today = datetime.datetime.now().date()
  today_str = datetime.datetime.now().strftime("%d-%m-%Y")
  today_db = datetime.datetime.now().strftime("%Y-%m-%d")
  
  app.setStyle( styles[style] )
  
  db_name = "hmcc_" + startyear + "-" + endyear
  db = psycopg2.connect(database = db_name, user='postgres')
  
  #translator = QtCore.QTranslator(app)
  #translator.load("ui/hmcc_ml.qm"),
  #app.installTranslator(translator)
  
  hmcc = Hmcc()

  app.exec_()
