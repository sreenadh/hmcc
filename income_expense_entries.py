# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

import sys

from ui.income_expense_entries_ui import Ui_in_exp_entries

class Income_Expense_Entries(QtGui.QWidget, Ui_in_exp_entries):

    def __init__(self):
      QtGui.QWidget.__init__(self)