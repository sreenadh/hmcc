# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import datetime
  
def findWeek(date):
  STARTDATE=date
  date_format = "%d-%m-%Y"
  startdate = datetime.datetime.strptime(STARTDATE,date_format).date()
  WeekNumber = ( datetime.date.today() - startdate ).days
  WeekNumber = ( WeekNumber / 7 ) + 1
  end_date = startdate + datetime.timedelta(51 * 7)
  return [ WeekNumber, startdate ]
  
class Functions():
  
  def clear_Layout(self):
      self.removeFrame()
      self.frame.setEnabled(True)
      self.frame_6.setVisible(True)
      self.actionFrame = None
      self.flag = 0
      
  def showBack(self, fn ):
      self.connect(self.actionFrame.btnBack,QtCore.SIGNAL('clicked()'),fn)
  
  def disableMenu(self):
      self.frame.setEnabled(False)
      self.frame_6.setVisible(False)
      
  def showActions(self, actionFrame):
      self.qleMessageBox.setText("")
      self.qleMessageBox.setVisible(False)
      self.removeFrame()
      self.actionFrame = actionFrame
      self.actionFrame.setupUi(self.actionFrame)
      self.disableMenu()
      self.layout_widget.addWidget(self.actionFrame)
    
  def signal_connect(self, sender, fn ):
    self.connect(sender,QtCore.SIGNAL('clicked()'),fn)
    
  def removeFrame(self):
    if self.actionFrame != None:
      self.actionFrame.close()
      del self.actionFrame
      
  def showMessageBox(self, message,type=1):
    self.qleMessageBox.setText(message)
    self.qleMessageBox.setVisible(True)
    if type==1:
      self.qleMessageBox.setStyleSheet("color:green;background-color:yellow;font:bold 18pt Ubuntu;")
    else:
      self.qleMessageBox.setStyleSheet("color:red;background-color:yellow;font:bold 18pt Ubuntu;")
      
  def hideMessageBox(self):
    self.qleMessageBox.setVisible(False)
    self.qleMessageBox.setText("")