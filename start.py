# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtCore,QtGui

from splash import Splash
from hmcc import Hmcc


if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	
	splash = Splash()

	hmcc = Hmcc()

	splash.finish(hmcc)
	
	del splash

	app.exec_()