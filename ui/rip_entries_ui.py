# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rip_entries.ui'
#
# Created: Fri Jan 18 15:50:37 2013
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_rip_entries(object):
    def setupUi(self, rip_entries):
        rip_entries.setObjectName(_fromUtf8("rip_entries"))
        rip_entries.resize(715, 311)
        self.verticalLayout_2 = QtGui.QVBoxLayout(rip_entries)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.label = QtGui.QLabel(rip_entries)
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setStyleSheet(_fromUtf8("color:rgb(170, 0, 0)"))
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout_2.addWidget(self.label)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.tbl_death_entries = QtGui.QTableWidget(rip_entries)
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.tbl_death_entries.setFont(font)
        self.tbl_death_entries.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.tbl_death_entries.setTabKeyNavigation(False)
        self.tbl_death_entries.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.tbl_death_entries.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.tbl_death_entries.setObjectName(_fromUtf8("tbl_death_entries"))
        self.tbl_death_entries.setColumnCount(6)
        self.tbl_death_entries.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.tbl_death_entries.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tbl_death_entries.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tbl_death_entries.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tbl_death_entries.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.tbl_death_entries.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.tbl_death_entries.setHorizontalHeaderItem(5, item)
        self.tbl_death_entries.verticalHeader().setVisible(False)
        self.horizontalLayout.addWidget(self.tbl_death_entries)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.btnBack = QtGui.QPushButton(rip_entries)
        self.btnBack.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName(_fromUtf8("btnBack"))
        self.verticalLayout.addWidget(self.btnBack)
        self.btnDelete = QtGui.QPushButton(rip_entries)
        self.btnDelete.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.btnDelete.setFont(font)
        self.btnDelete.setObjectName(_fromUtf8("btnDelete"))
        self.verticalLayout.addWidget(self.btnDelete)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.retranslateUi(rip_entries)
        QtCore.QMetaObject.connectSlotsByName(rip_entries)

    def retranslateUi(self, rip_entries):
        rip_entries.setWindowTitle(QtGui.QApplication.translate("rip_entries", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("rip_entries", "<html><head/><body><p>Death entries</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        item = self.tbl_death_entries.horizontalHeaderItem(0)
        item.setText(QtGui.QApplication.translate("rip_entries", "id", None, QtGui.QApplication.UnicodeUTF8))
        item = self.tbl_death_entries.horizontalHeaderItem(1)
        item.setText(QtGui.QApplication.translate("rip_entries", "Sl", None, QtGui.QApplication.UnicodeUTF8))
        item = self.tbl_death_entries.horizontalHeaderItem(2)
        item.setText(QtGui.QApplication.translate("rip_entries", "Date", None, QtGui.QApplication.UnicodeUTF8))
        item = self.tbl_death_entries.horizontalHeaderItem(3)
        item.setText(QtGui.QApplication.translate("rip_entries", "Book Number", None, QtGui.QApplication.UnicodeUTF8))
        item = self.tbl_death_entries.horizontalHeaderItem(4)
        item.setText(QtGui.QApplication.translate("rip_entries", "Name", None, QtGui.QApplication.UnicodeUTF8))
        item = self.tbl_death_entries.horizontalHeaderItem(5)
        item.setText(QtGui.QApplication.translate("rip_entries", "Age", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("rip_entries", "&Back", None, QtGui.QApplication.UnicodeUTF8))
        self.btnDelete.setText(QtGui.QApplication.translate("rip_entries", "&Delete", None, QtGui.QApplication.UnicodeUTF8))

