# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'reports.ui'
#
# Created: Wed Aug 10 11:03:46 2011
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Reports_Actions(QtGui.QWidget):
    def setupUi(self, Reports_Actions):
        Reports_Actions.setObjectName(_fromUtf8("Reports_Actions"))
        Reports_Actions.resize(505, 100)
        self.horizontalLayout = QtGui.QHBoxLayout(Reports_Actions)
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(14, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnMemberWise = QtGui.QPushButton(Reports_Actions)
        self.btnMemberWise.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnMemberWise.setFont(font)
        self.btnMemberWise.setObjectName(_fromUtf8("btnMemberWise"))
        self.horizontalLayout.addWidget(self.btnMemberWise)
        self.btnAllMembers = QtGui.QPushButton(Reports_Actions)
        self.btnAllMembers.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnAllMembers.setFont(font)
        self.btnAllMembers.setObjectName(_fromUtf8("btnAllMembers"))
        self.horizontalLayout.addWidget(self.btnAllMembers)
        self.btnWeeklyReport = QtGui.QPushButton(Reports_Actions)
        self.btnWeeklyReport.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnWeeklyReport.setFont(font)
        self.btnWeeklyReport.setObjectName(_fromUtf8("btnWeeklyReport"))
        self.horizontalLayout.addWidget(self.btnWeeklyReport)
        self.btnBack = QtGui.QPushButton(Reports_Actions)
        self.btnBack.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName(_fromUtf8("btnBack"))
        self.horizontalLayout.addWidget(self.btnBack)
        spacerItem1 = QtGui.QSpacerItem(12, 97, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)

        self.retranslateUi(Reports_Actions)
        QtCore.QMetaObject.connectSlotsByName(Reports_Actions)

    def retranslateUi(self, Reports_Actions):
        Reports_Actions.setWindowTitle(QtGui.QApplication.translate("Reports_Actions", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.btnMemberWise.setText(QtGui.QApplication.translate("Reports_Actions", "M&emberwise", None, QtGui.QApplication.UnicodeUTF8))
        self.btnAllMembers.setText(QtGui.QApplication.translate("Reports_Actions", "&All members", None, QtGui.QApplication.UnicodeUTF8))
        self.btnWeeklyReport.setText(QtGui.QApplication.translate("Reports_Actions", "&Weekly", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("Reports_Actions", "&Back", None, QtGui.QApplication.UnicodeUTF8))

