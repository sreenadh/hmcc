# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'member.ui'
#
# Created: Sat Aug 27 20:47:42 2011
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Member_Actions(object):
    def setupUi(self, Member_Actions):
        Member_Actions.setObjectName(_fromUtf8("Member_Actions"))
        Member_Actions.resize(336, 100)
        self.horizontalLayout = QtGui.QHBoxLayout(Member_Actions)
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(11, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnMemberAdd = QtGui.QPushButton(Member_Actions)
        self.btnMemberAdd.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnMemberAdd.setFont(font)
        self.btnMemberAdd.setObjectName(_fromUtf8("btnMemberAdd"))
        self.horizontalLayout.addWidget(self.btnMemberAdd)
        self.btnMemberList = QtGui.QPushButton(Member_Actions)
        self.btnMemberList.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnMemberList.setFont(font)
        self.btnMemberList.setObjectName(_fromUtf8("btnMemberList"))
        self.horizontalLayout.addWidget(self.btnMemberList)
        self.btnRip = QtGui.QPushButton(Member_Actions)
        self.btnRip.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnRip.setFont(font)
        self.btnRip.setObjectName(_fromUtf8("btnRip"))
        self.horizontalLayout.addWidget(self.btnRip)
        self.btnBack = QtGui.QPushButton(Member_Actions)
        self.btnBack.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName(_fromUtf8("btnBack"))
        self.horizontalLayout.addWidget(self.btnBack)
        spacerItem1 = QtGui.QSpacerItem(11, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)

        self.retranslateUi(Member_Actions)
        QtCore.QMetaObject.connectSlotsByName(Member_Actions)

    def retranslateUi(self, Member_Actions):
        Member_Actions.setWindowTitle(QtGui.QApplication.translate("Member_Actions", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.btnMemberAdd.setText(QtGui.QApplication.translate("Member_Actions", "&Add", None, QtGui.QApplication.UnicodeUTF8))
        self.btnMemberList.setText(QtGui.QApplication.translate("Member_Actions", "&List", None, QtGui.QApplication.UnicodeUTF8))
        self.btnRip.setText(QtGui.QApplication.translate("Member_Actions", "&Death", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("Member_Actions", "&Back", None, QtGui.QApplication.UnicodeUTF8))

