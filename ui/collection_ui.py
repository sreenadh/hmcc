# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'collection.ui'
#
# Created: Sat Aug 27 20:47:37 2011
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Collection_Actions(object):
    def setupUi(self, Collection_Actions):
        Collection_Actions.setObjectName(_fromUtf8("Collection_Actions"))
        Collection_Actions.resize(503, 100)
        self.horizontalLayout = QtGui.QHBoxLayout(Collection_Actions)
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(30, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnCollectionEntry = QtGui.QPushButton(Collection_Actions)
        self.btnCollectionEntry.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnCollectionEntry.setFont(font)
        self.btnCollectionEntry.setObjectName(_fromUtf8("btnCollectionEntry"))
        self.horizontalLayout.addWidget(self.btnCollectionEntry)
        self.btnCollectionList = QtGui.QPushButton(Collection_Actions)
        self.btnCollectionList.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.btnCollectionList.setFont(font)
        self.btnCollectionList.setObjectName(_fromUtf8("btnCollectionList"))
        self.horizontalLayout.addWidget(self.btnCollectionList)
        self.btnIncome = QtGui.QPushButton(Collection_Actions)
        self.btnIncome.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnIncome.setFont(font)
        self.btnIncome.setObjectName(_fromUtf8("btnIncome"))
        self.horizontalLayout.addWidget(self.btnIncome)
        self.btnBack = QtGui.QPushButton(Collection_Actions)
        self.btnBack.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName(_fromUtf8("btnBack"))
        self.horizontalLayout.addWidget(self.btnBack)
        spacerItem1 = QtGui.QSpacerItem(30, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)

        self.retranslateUi(Collection_Actions)
        QtCore.QMetaObject.connectSlotsByName(Collection_Actions)

    def retranslateUi(self, Collection_Actions):
        Collection_Actions.setWindowTitle(QtGui.QApplication.translate("Collection_Actions", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.btnCollectionEntry.setText(QtGui.QApplication.translate("Collection_Actions", "&Add", None, QtGui.QApplication.UnicodeUTF8))
        self.btnCollectionList.setText(QtGui.QApplication.translate("Collection_Actions", "&List", None, QtGui.QApplication.UnicodeUTF8))
        self.btnIncome.setText(QtGui.QApplication.translate("Collection_Actions", "&Income && Expense", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("Collection_Actions", "&Back", None, QtGui.QApplication.UnicodeUTF8))

