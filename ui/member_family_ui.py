# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'member_family.ui'
#
# Created: Sat Aug 20 16:08:39 2011
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Family_Members(object):
    def setupUi(self, Family_Members):
        Family_Members.setObjectName(_fromUtf8("Family_Members"))
        Family_Members.resize(286, 302)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Family_Members.sizePolicy().hasHeightForWidth())
        Family_Members.setSizePolicy(sizePolicy)
        self.verticalLayout_2 = QtGui.QVBoxLayout(Family_Members)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.label = QtGui.QLabel(Family_Members)
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.label.setFont(font)
        self.label.setStyleSheet(_fromUtf8("color:rgb(170, 0, 0)"))
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout_2.addWidget(self.label)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setSpacing(5)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.qle1 = QtGui.QLineEdit(Family_Members)
        self.qle1.setObjectName(_fromUtf8("qle1"))
        self.verticalLayout.addWidget(self.qle1)
        self.qle2 = QtGui.QLineEdit(Family_Members)
        self.qle2.setObjectName(_fromUtf8("qle2"))
        self.verticalLayout.addWidget(self.qle2)
        self.qle3 = QtGui.QLineEdit(Family_Members)
        self.qle3.setObjectName(_fromUtf8("qle3"))
        self.verticalLayout.addWidget(self.qle3)
        self.qle4 = QtGui.QLineEdit(Family_Members)
        self.qle4.setObjectName(_fromUtf8("qle4"))
        self.verticalLayout.addWidget(self.qle4)
        self.qle5 = QtGui.QLineEdit(Family_Members)
        self.qle5.setObjectName(_fromUtf8("qle5"))
        self.verticalLayout.addWidget(self.qle5)
        self.qle6 = QtGui.QLineEdit(Family_Members)
        self.qle6.setObjectName(_fromUtf8("qle6"))
        self.verticalLayout.addWidget(self.qle6)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnSubmit = QtGui.QPushButton(Family_Members)
        self.btnSubmit.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.btnSubmit.setFont(font)
        self.btnSubmit.setObjectName(_fromUtf8("btnSubmit"))
        self.horizontalLayout.addWidget(self.btnSubmit)
        self.btnBack = QtGui.QPushButton(Family_Members)
        self.btnBack.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName(_fromUtf8("btnBack"))
        self.horizontalLayout.addWidget(self.btnBack)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        spacerItem2 = QtGui.QSpacerItem(20, 31, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem2)

        self.retranslateUi(Family_Members)
        QtCore.QMetaObject.connectSlotsByName(Family_Members)
        Family_Members.setTabOrder(self.qle1, self.qle2)
        Family_Members.setTabOrder(self.qle2, self.qle3)
        Family_Members.setTabOrder(self.qle3, self.qle4)
        Family_Members.setTabOrder(self.qle4, self.qle5)
        Family_Members.setTabOrder(self.qle5, self.qle6)
        Family_Members.setTabOrder(self.qle6, self.btnSubmit)
        Family_Members.setTabOrder(self.btnSubmit, self.btnBack)

    def retranslateUi(self, Family_Members):
        Family_Members.setWindowTitle(QtGui.QApplication.translate("Family_Members", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Family_Members", "Family Members", None, QtGui.QApplication.UnicodeUTF8))
        self.btnSubmit.setText(QtGui.QApplication.translate("Family_Members", "&Submit", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("Family_Members", "&Back", None, QtGui.QApplication.UnicodeUTF8))

