# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'admin.ui'
#
# Created: Sat Aug 27 20:47:26 2011
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Admin_Actions(object):
    def setupUi(self, Admin_Actions):
        Admin_Actions.setObjectName(_fromUtf8("Admin_Actions"))
        Admin_Actions.resize(335, 100)
        self.horizontalLayout = QtGui.QHBoxLayout(Admin_Actions)
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem = QtGui.QSpacerItem(28, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnBackup = QtGui.QPushButton(Admin_Actions)
        self.btnBackup.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnBackup.setFont(font)
        self.btnBackup.setObjectName(_fromUtf8("btnBackup"))
        self.horizontalLayout.addWidget(self.btnBackup)
        self.btnYearEnd = QtGui.QPushButton(Admin_Actions)
        self.btnYearEnd.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("FreeSans"))
        font.setPointSize(20)
        font.setWeight(75)
        font.setBold(True)
        self.btnYearEnd.setFont(font)
        self.btnYearEnd.setObjectName(_fromUtf8("btnYearEnd"))
        self.horizontalLayout.addWidget(self.btnYearEnd)
        self.btnBack = QtGui.QPushButton(Admin_Actions)
        self.btnBack.setMinimumSize(QtCore.QSize(0, 100))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setWeight(75)
        font.setBold(True)
        self.btnBack.setFont(font)
        self.btnBack.setObjectName(_fromUtf8("btnBack"))
        self.horizontalLayout.addWidget(self.btnBack)
        spacerItem1 = QtGui.QSpacerItem(28, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)

        self.retranslateUi(Admin_Actions)
        QtCore.QMetaObject.connectSlotsByName(Admin_Actions)

    def retranslateUi(self, Admin_Actions):
        Admin_Actions.setWindowTitle(QtGui.QApplication.translate("Admin_Actions", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBackup.setText(QtGui.QApplication.translate("Admin_Actions", "B&ackup", None, QtGui.QApplication.UnicodeUTF8))
        self.btnYearEnd.setText(QtGui.QApplication.translate("Admin_Actions", "&New Year", None, QtGui.QApplication.UnicodeUTF8))
        self.btnBack.setText(QtGui.QApplication.translate("Admin_Actions", "&Back", None, QtGui.QApplication.UnicodeUTF8))

