--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: hmcc_qt; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE hmcc_qt IS 'QT testing';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: collection; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE collection (
    membershipno integer,
    date character varying(25),
    amount integer,
    receiptno integer DEFAULT nextval(('rcptno'::text)::regclass),
    weekno integer
);


ALTER TABLE public.collection OWNER TO postgres;

--
-- Name: family; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE family (
    membershipno integer,
    fname character varying(25)
);


ALTER TABLE public.family OWNER TO postgres;

--
-- Name: memberlist; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE memberlist (
    membershipno integer,
    name character varying(25),
    house character varying(25),
    road character varying(25),
    post character varying(25),
    town character varying(25),
    city character varying(25),
    pin character varying(10)
);


ALTER TABLE public.memberlist OWNER TO postgres;

--
-- Name: payment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE payment (
    membershipno integer,
    date character varying(25),
    payamount integer
);


ALTER TABLE public.payment OWNER TO postgres;

--
-- Name: rcptno; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rcptno
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.rcptno OWNER TO postgres;

--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

