--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: hmcc_2010-2011; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE "hmcc_2010-2011" IS 'QT testing';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: collection; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE collection (
    membershipno bigint,
    date character varying(25),
    amount integer,
    receiptno integer DEFAULT nextval(('rcptno'::text)::regclass),
    weekno integer
);


ALTER TABLE public.collection OWNER TO postgres;

--
-- Name: collection_bak; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE collection_bak (
    modified_date character varying(50),
    membershipno integer,
    date character varying(25),
    amount integer,
    receiptno integer,
    weekno integer
);


ALTER TABLE public.collection_bak OWNER TO postgres;

--
-- Name: family; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE family (
    membershipno bigint,
    fname character varying(25)
);


ALTER TABLE public.family OWNER TO postgres;

--
-- Name: income_expense; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE income_expense (
    id integer NOT NULL,
    debit bigint,
    credit bigint,
    date character varying(20),
    description character varying(100)
);


ALTER TABLE public.income_expense OWNER TO postgres;

--
-- Name: income_expense_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE income_expense_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.income_expense_id_seq OWNER TO postgres;

--
-- Name: income_expense_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE income_expense_id_seq OWNED BY income_expense.id;


--
-- Name: memberlist; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE memberlist (
    membershipno bigint,
    name character varying(25),
    house character varying(25),
    road character varying(25),
    post character varying(25),
    town character varying(25),
    city character varying(25),
    pin character varying(10)
);


ALTER TABLE public.memberlist OWNER TO postgres;

--
-- Name: payment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE payment (
    membershipno integer,
    date character varying(25),
    payamount integer
);


ALTER TABLE public.payment OWNER TO postgres;

--
-- Name: rcptno; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rcptno
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.rcptno OWNER TO postgres;

--
-- Name: rip; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rip (
    id integer NOT NULL,
    membershipno bigint,
    date character varying(25),
    name character varying(25),
    age integer NOT NULL,
    address text
);


ALTER TABLE public.rip OWNER TO postgres;

--
-- Name: rip_age_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rip_age_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.rip_age_seq OWNER TO postgres;

--
-- Name: rip_age_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE rip_age_seq OWNED BY rip.age;


--
-- Name: rip_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.rip_id_seq OWNER TO postgres;

--
-- Name: rip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE rip_id_seq OWNED BY rip.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE income_expense ALTER COLUMN id SET DEFAULT nextval('income_expense_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE rip ALTER COLUMN id SET DEFAULT nextval('rip_id_seq'::regclass);


--
-- Name: age; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE rip ALTER COLUMN age SET DEFAULT nextval('rip_age_seq'::regclass);


--
-- Name: income_expense_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY income_expense
    ADD CONSTRAINT income_expense_pkey PRIMARY KEY (id);


--
-- Name: rip_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rip
    ADD CONSTRAINT rip_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

