# -*- coding: utf-8 -*-

import psycopg2
import sys, locale, ConfigParser, datetime, time, os
from string import *

def execute_query(query, data=""):
      db = psycopg2.connect(database = 'hmcc_2012-2013', user='postgres')
      c = db.cursor()
      try:
        c.execute( query, data )
        db.commit()
      except Exception, e:
        print e
        db.rollback()
        return False
      else:
        return c

def printReport():
      D = []
      query = """SELECT m.membershipno,name,SUM(amount) AS amount 
              FROM MemberList m 
              LEFT JOIN collection c ON(c.membershipno=m.membershipno) 
              GROUP BY m.membershipno,m.name
              ORDER BY membershipno LIMIT 30 OFFSET 696"""
      c = execute_query( query )
      for x in c.fetchall():
          D.append(x)
      abc=[]
      abc.append(['Sl', 'Name','Amount','Balance'])
      lp = open("/dev/usb/lp0", "w")
      for x in D:
          if x[2]:
            if x[2]<440:
              balance=0
            else:
              balance=x[2]-452.63
          else:
            balance=0
          abc.append([x[0],strip(upper(x[1])),x[2],balance])

          s="\n\n"
          s+="\tHMCC - Mattancherry\n"
          s+="\tDate         : " + today_str + "\n"
          s+="\tBook No.     : " + str(x[0]) + "\n"
          s+="\tTotal deaths : 71\n"
          s+="\tShare        : 392.63\n"
          s+="\tOp. Expense  : 60\n"
          s+="\tDeposit      : " + str(x[2]) + "\n"
          s+="\tTotal Expense: 452.63\n"
          s+="\tBalance      : " + str(balance)
          s+="\n"
          lp.write(s)
      lp.close()

if __name__ == "__main__":
	print "printing....\n"
        today_str = datetime.datetime.now().strftime("%d-%m-%Y")
	printReport()
