# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtCore,QtGui

from ui.splash_ui import Ui_splash

class Splash(QtGui.QSplashScreen, Ui_splash):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        screen = QtGui.QDesktopWidget().screenGeometry()
        self.setupUi(self)
        size =  self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)
        self.setWindowFlags(QtCore.Qt.SplashScreen)
        self.show()
        
if __name__ == "__main__":
  app = QtGui.QApplication(sys.argv)
  splash = Splash()
  splash.show()

  app.exec_()